package md.utm.labs.pr;

public class Runner {
    public static void main(String[] args) throws InterruptedException {
	Thread t = new Thread(new Greeter());
	t.start();
	t.join();
	Thread t2 = new Worker();
	t2.start();
	t2.join();
	System.out.println("The greeter successfully executed.");
    }
}
