package md.utm.labs.pr;

import org.junit.*;
import static org.junit.Assert.*;
import java.io.*;

public class AppTest {
    private Runner runner;
    
    private PrintStream oldOut;
    private ByteArrayOutputStream stream = new ByteArrayOutputStream();
    
    @Before
    public void setUp() {
        oldOut = System.out;
        System.setOut(new PrintStream(stream));
    }
    
    public void tearDown() {
        System.setOut(oldOut);
    }
    
    @Test
    public void test() throws InterruptedException {
        runner.main(new String[] {});
        assertEquals("Hello, world!\nWorking...\nThe greeter successfully executed.\n", new String(stream.toByteArray()));
    }
}
